/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import com.google.gson.reflect.TypeToken;
import de.klarcloudservice.KlarCloudAPISpigot;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.signaddon.SignSelector;
import de.klarcloudservice.signs.KlarCloudSign;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 12.12.2018
 */

public class PacketInRemoveSign implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (KlarCloudAPISpigot.getInstance().getInternalCloudNetwork().isSigns()) {
            SignSelector.getInstance().handleSignRemove(configuration.getValue("klarCloudSign", new TypeToken<KlarCloudSign>() {
            }.getType()));
        }
    }
}
