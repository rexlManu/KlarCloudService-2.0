/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.bootstrap.SpigotBootstrap;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.List;
import java.util.UUID;

/**
 * @author _Klaro | Pasqual K. / created on 14.12.2018
 */

public class PacketInPlayerAccepted implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (configuration.getBooleanValue("accepted")) {
            SpigotBootstrap.getInstance().getAcceptedPlayers().add(configuration.getValue("uuid", UUID.class));
            SpigotBootstrap.getInstance().getServer().getScheduler().runTaskLaterAsynchronously(SpigotBootstrap.getInstance(), () -> {
                SpigotBootstrap.getInstance().getAcceptedPlayers().remove(configuration.getValue("uuid", UUID.class));
            }, 40L);
        }
    }
}
