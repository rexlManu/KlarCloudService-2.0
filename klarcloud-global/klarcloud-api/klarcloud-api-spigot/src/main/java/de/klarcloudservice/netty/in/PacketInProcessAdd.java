/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.bootstrap.SpigotBootstrap;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.internal.events.CloudProxyAddEvent;
import de.klarcloudservice.internal.events.CloudServerAddEvent;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.TypeTokenAdaptor;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 07.11.2018
 */

public class PacketInProcessAdd implements NettyAdaptor {
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        if (configuration.contains("serverInfo")) {
            final ServerInfo serverInfo = configuration.getValue("serverInfo", TypeTokenAdaptor.getServerInfoType());
            SpigotBootstrap.getInstance().getServer().getPluginManager().callEvent(new CloudServerAddEvent(serverInfo));
        } else {
            final ProxyInfo proxyInfo = configuration.getValue("proxyInfo", TypeTokenAdaptor.getProxyInfoType());
            SpigotBootstrap.getInstance().getServer().getPluginManager().callEvent(new CloudProxyAddEvent(proxyInfo));
        }
    }
}
