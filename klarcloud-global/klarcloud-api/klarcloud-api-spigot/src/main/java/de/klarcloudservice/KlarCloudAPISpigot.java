/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice;

import de.klarcloudservice.bootstrap.SpigotBootstrap;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.event.EventManager;
import de.klarcloudservice.logging.KlarCloudConsoleLogger;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.meta.proxy.ProxyGroup;
import de.klarcloudservice.meta.server.ServerGroup;
import de.klarcloudservice.meta.startup.ServerStartupInfo;
import de.klarcloudservice.netty.NettyHandler;
import de.klarcloudservice.netty.NettySocketClient;
import de.klarcloudservice.netty.channel.ChannelHandler;
import de.klarcloudservice.netty.in.*;
import de.klarcloudservice.netty.packets.PacketOutServerInfoUpdate;
import de.klarcloudservice.netty.packets.PacketOutStartGameServer;
import de.klarcloudservice.netty.packets.PacketOutStartProxy;
import de.klarcloudservice.utility.TypeTokenAdaptor;
import de.klarcloudservice.utility.cloudsystem.InternalCloudNetwork;
import de.klarcloudservice.utility.cloudsystem.KlarCloudAddresses;
import lombok.Getter;
import lombok.Setter;

import javax.management.InstanceAlreadyExistsException;
import java.nio.file.Paths;

/**
 * @author _Klaro | Pasqual K. / created on 09.12.2018
 */

@Getter
@Setter
public class KlarCloudAPISpigot {
    @Getter
    public static KlarCloudAPISpigot instance;

    private final NettySocketClient nettySocketClient;
    private final NettyHandler nettyHandler = new NettyHandler();
    private final ChannelHandler channelHandler = new ChannelHandler();

    private final ServerStartupInfo serverStartupInfo;
    private ServerInfo serverInfo;
    private InternalCloudNetwork internalCloudNetwork = new InternalCloudNetwork();

    /**
     * Creates a new KlarCloud Spigot instance
     *
     * @throws Throwable
     */
    public KlarCloudAPISpigot() throws Throwable {
        if (instance == null)
            instance = this;
        else
            throw new InstanceAlreadyExistsException();
        KlarCloudLibrary.sendHeader();

        Configuration configuration = Configuration.loadConfiguration(Paths.get("klarcloud/config.json"));

        final KlarCloudAddresses klarCloudAddresses = configuration.getValue("address", TypeTokenAdaptor.getKlarCloudAddressesType());
        new KlarCloudLibraryService(new KlarCloudConsoleLogger(false), configuration.getStringValue("controllerKey"), klarCloudAddresses.getHost(), new EventManager());

        this.serverStartupInfo = configuration.getValue("startupInfo", TypeTokenAdaptor.getServerStartupInfoType());
        this.serverInfo = configuration.getValue("info", TypeTokenAdaptor.getServerInfoType());

        this.nettyHandler.registerHandler("InitializeCloudNetwork", new PacketInInitializeInternal());
        this.nettyHandler.registerHandler("UpdateAll", new PacketInUpdateAll());
        this.nettyHandler.registerHandler("ProcessAdd", new PacketInProcessAdd());
        this.nettyHandler.registerHandler("ProcessRemove", new PacketInProcessRemove());
        this.nettyHandler.registerHandler("ServerInfoUpdate", new PacketInServerInfoUpdate());
        this.nettyHandler.registerHandler("Signs", new PacketInRequestSigns());
        this.nettyHandler.registerHandler("RemoveSign", new PacketInRemoveSign());
        this.nettyHandler.registerHandler("CreateSign", new PacketInCreateSign());
        this.nettyHandler.registerHandler("PlayerAccepted", new PacketInPlayerAccepted());

        this.nettySocketClient = new NettySocketClient();
        this.nettySocketClient.connect(
                klarCloudAddresses, nettyHandler, channelHandler, configuration.getBooleanValue("ssl"),
                configuration.getStringValue("controllerKey"), this.serverStartupInfo.getName()
        );
    }

    public void setCloudServerMOTDandSendUpdatePacket(final String newMOTD) {
        this.serverInfo.setMotd(newMOTD);
        this.channelHandler.sendPacketSynchronized("KlarCloudController", new PacketOutServerInfoUpdate(serverInfo));
    }

    public void startGameServer(final String serverGroupName) {
        this.startGameServer(serverGroupName, new Configuration());
    }

    public void startGameServer(final String serverGroupName, final Configuration preConfig) {
        final ServerGroup serverGroup = this.internalCloudNetwork.getServerGroups().getOrDefault(serverGroupName, null);
        this.startGameServer(serverGroup, preConfig);
    }

    public void startGameServer(final ServerGroup serverGroup) {
        this.startGameServer(serverGroup.getName(), new Configuration());
    }

    public void startGameServer(final ServerGroup serverGroup, final Configuration preConfig) {
        this.channelHandler.sendPacketAsynchronous("KlarCloudController", new PacketOutStartGameServer(serverGroup, preConfig));
    }

    public void startProxy(final String proxyGroupName) {
        this.startProxy(proxyGroupName, new Configuration());
    }

    public void startProxy(final String proxyGroupName, final Configuration preConfig) {
        final ProxyGroup proxyGroup = this.internalCloudNetwork.getProxyGroups().getOrDefault(proxyGroupName, null);
        this.channelHandler.sendPacketAsynchronous("KlarCloudController", new PacketOutStartProxy(proxyGroup, preConfig));
    }

    public void startProxy(final ProxyGroup proxyGroup) {
        this.startProxy(proxyGroup.getName());
    }

    public void startProxy(final ProxyGroup proxyGroup, final Configuration preConfig) {
        this.startProxy(proxyGroup.getName(), preConfig);
    }

    /**
     * @deprecated
     */
    @Deprecated
    public void removeInternalProcess() {
        SpigotBootstrap.getInstance().onDisable();
    }
}
