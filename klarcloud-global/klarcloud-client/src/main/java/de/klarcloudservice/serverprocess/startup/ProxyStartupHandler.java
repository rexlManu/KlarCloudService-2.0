/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.serverprocess.startup;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.meta.CloudProcess;
import de.klarcloudservice.meta.enums.TemplateBackend;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.proxy.versions.ProxyVersions;
import de.klarcloudservice.meta.startup.ProxyStartupInfo;
import de.klarcloudservice.meta.startup.stages.ProcessStartupStage;
import de.klarcloudservice.netty.packets.PacketOutAddProcess;
import de.klarcloudservice.netty.packets.PacketOutRemoveProcess;
import de.klarcloudservice.netty.packets.PacketOutSendControllerConsoleMessage;
import de.klarcloudservice.netty.packets.PacketOutUpdateInternalCloudNetwork;
import de.klarcloudservice.template.TemplatePreparer;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.utility.files.DownloadManager;
import de.klarcloudservice.utility.files.FileUtils;
import de.klarcloudservice.utility.files.ZoneInformationProtocolUtility;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @author _Klaro | Pasqual K. / created on 30.10.2018
 */

@Getter
public class ProxyStartupHandler {
    private ProxyStartupInfo proxyStartupInfo;
    private Path path;
    private Process process;
    private int port;

    private ProcessStartupStage processStartupStage;

    /**
     * Creates a instance of a ProxyStartupHandler
     *
     * @param proxyStartupInfo
     */
    public ProxyStartupHandler(final ProxyStartupInfo proxyStartupInfo) {
        this.processStartupStage = ProcessStartupStage.WAITING;
        this.proxyStartupInfo = proxyStartupInfo;
        this.path = Paths.get("klarcloud/temp/proxies/" + proxyStartupInfo.getName() + "-" + proxyStartupInfo.getUid());
    }

    /**
     * Starts the BungeeCord
     *
     * @return {@code true} if the Client could start the BungeeCord
     * or {@code false} if the Client couldn't start the BungeeCord
     */
    public boolean bootstrap() {
        FileUtils.deleteFullDirectory(path);

        this.processStartupStage = ProcessStartupStage.COPY;
        this.sendMessage("KlarCloud copies custom Template to \"" + path + "\", this may take a long time...");
        if (proxyStartupInfo.getProxyGroup().getTemplate().getTemplateBackend().equals(TemplateBackend.URL)
                && proxyStartupInfo.getProxyGroup().getTemplate().getTemplate_url() != null) {
            new TemplatePreparer(path + "/template.zip").loadTemplate(proxyStartupInfo.getProxyGroup().getTemplate().getTemplate_url());
            ZoneInformationProtocolUtility.extract(path + "/template.zip", path + "");
        } else if (proxyStartupInfo.getProxyGroup().getTemplate().getTemplateBackend().equals(TemplateBackend.CLIENT)) {
            FileUtils.copyAllFiles(Paths.get("klarcloud/templates/" + proxyStartupInfo.getProxyGroup().getName()), path + StringUtil.EMPTY);
        } else {
            return false;
        }

        if (!Files.exists(Paths.get(path + "/plugins")))
            FileUtils.createDirectory(Paths.get(path + "/plugins"));

        FileUtils.createDirectory(Paths.get(path + "/klarcloud"));

        FileUtils.copyAllFiles(Paths.get("klarcloud/default/proxies"), path + StringUtil.EMPTY);

        this.processStartupStage = ProcessStartupStage.PREPARING;
        this.port = KlarCloudClient.getInstance().getInternalCloudNetwork()
                .getServerProcessManager().nextFreePort(proxyStartupInfo.getProxyGroup().getStartPort());

        if (!Files.exists(Paths.get(path + "/server-icon.png")))
            FileUtils.copyCompiledFile("klarcloud/server-icon.png", path + "/server-icon.png");

        if (!Files.exists(Paths.get(path + "/config.yml")))
            FileUtils.copyCompiledFile("klarcloud/config.yml", path + "/config.yml");

        if (!Files.exists(Paths.get(path + "/BungeeCord.jar"))) {
            if (!Files.exists(Paths.get("klarcloud/files/" + ProxyVersions.getAsJarFileName(this.proxyStartupInfo.getProxyGroup().getProxyVersions())))) {
                DownloadManager.downloadAndDisconnect(
                        this.proxyStartupInfo.getProxyGroup().getProxyVersions().getName(),
                        this.proxyStartupInfo.getProxyGroup().getProxyVersions().getUrl(),
                        "klarcloud/files/" + ProxyVersions.getAsJarFileName(this.proxyStartupInfo.getProxyGroup().getProxyVersions())
                );
            }

            FileUtils.copyFile("klarcloud/files/" + ProxyVersions.getAsJarFileName(this.proxyStartupInfo.getProxyGroup().getProxyVersions()), path + "/BungeeCord.jar");
        }

        if (proxyStartupInfo.getProxyGroup().getProxyConfig().isViaVersion() && !Files.exists(Paths.get(path + "plugins/ViaVersion.jar"))) {
            if (!Files.exists(Paths.get("klarcloud/files/ViaVersion.jar")))
                this.downloadViaVersion();

            FileUtils.copyFile("klarcloud/files/ViaVersion.jar", path + "/plugins/ViaVersion.jar");
        }

        if (!Files.exists(Paths.get("klarcloud/files/KlarCloudAPIBungee-" + StringUtil.BUNGEE_API_DOWNLOAD + ".jar"))) {
            DownloadManager.downloadSilentAndDisconnect(
                    "https://dl.klarcloudservice.de/download/latest/KlarCloudAPIBungee-" + StringUtil.BUNGEE_API_DOWNLOAD + ".jar",
                    "klarcloud/files/KlarCloudAPIBungee-" + StringUtil.BUNGEE_API_DOWNLOAD + ".jar"
            );

            final File dir = new File("klarcloud/files");
            if (dir.listFiles() != null) {
                Arrays.stream(dir.listFiles()).forEach(file -> {
                    if (file.getName().startsWith("KlarCloudAPIBungee")
                            && file.getName().endsWith(".jar")
                            && !file.getName().contains(StringUtil.BUNGEE_API_DOWNLOAD)) {
                        file.delete();
                    }
                });
            }
        }

        FileUtils.deleteFileIfExists(Paths.get(path + "/plugins/KlarCloudAPIBungee.jar"));
        FileUtils.copyFile("klarcloud/files/KlarCloudAPIBungee-" + StringUtil.BUNGEE_API_DOWNLOAD + ".jar", this.path + "/plugins/KlarCloudAPIBungee.jar");

        try {
            this.prepareConfiguration(new File(path + "/config.yml"), "\"" +
                    proxyStartupInfo.getProxyGroup().getProxyConfig().getIp() + ":" + port + "\"");
        } catch (final Throwable throwable) {
            StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Error while preparing proxy configuration, break", throwable);
            return false;
        }

        ProxyInfo proxyInfo = new ProxyInfo(
                new CloudProcess(proxyStartupInfo.getName(), proxyStartupInfo.getUid(), KlarCloudClient.getInstance().getClientConfiguration().getClientName(),
                        proxyStartupInfo.getId()),
                proxyStartupInfo.getProxyGroup(), proxyStartupInfo.getProxyGroup().getName(), KlarCloudClient.getInstance().getClientConfiguration().getStartIP(),
                this.port, 0, proxyStartupInfo.getProxyGroup().getMemory(), proxyStartupInfo.getProxyGroup().getProxyConfig().isMaintenance(), false,
                new ArrayList<>()
        );

        new Configuration()
                .addProperty("info", proxyInfo)
                .addProperty("address", KlarCloudClient.getInstance().getClientConfiguration().getKlarCloudAddresses())
                .addStringProperty("controllerKey", KlarCloudClient.getInstance().getClientConfiguration().getControllerKey())
                .addBooleanProperty("ssl", KlarCloudClient.getInstance().getNettySocketClient().getSslContext() != null)
                .addProperty("startupInfo", proxyStartupInfo)

                .saveAsConfigurationFile(Paths.get(path + "/klarcloud/config.json"));

        this.processStartupStage = ProcessStartupStage.START;
        final String[] cmd = new String[]
                {
                        StringUtil.JAVA,
                        "-XX:+UseG1GC",
                        "-XX:MaxGCPauseMillis=50",
                        "-XX:-UseAdaptiveSizePolicy",
                        "-XX:CompileThreshold=100",
                        "-Dio.netty.leakDetectionLevel=DISABLED",
                        "-Djline.terminal=jline.UnsupportedTerminal",
                        "-Dfile.encoding=UTF-8",
                        "-Xmx" + this.proxyStartupInfo.getProxyGroup().getMemory() + "M",
                        StringUtil.JAVA_JAR,
                        "BungeeCord.jar"
                };

        try {
            this.process = Runtime.getRuntime().exec(cmd, null, new File(path.toString()));
        } catch (final IOException ex) {
            StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Error while starting proxy process", ex);
            return false;
        }

        KlarCloudClient.getInstance().getInternalCloudNetwork().getServerProcessManager().registerProxyProcess(
                proxyStartupInfo.getUid(), proxyStartupInfo.getName(), proxyInfo, port
        );
        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutUpdateInternalCloudNetwork(KlarCloudClient.getInstance().getInternalCloudNetwork()), new PacketOutAddProcess(proxyInfo));

        KlarCloudClient.getInstance().getCloudProcessScreenService().registerProxyProcess(proxyStartupInfo.getName(), this);
        this.processStartupStage = ProcessStartupStage.DONE;
        return true;
    }

    /**
     * Checks if the BungeeCordProcess is alive
     *
     * @return {@code true} if the BungeeCordProcess is alive or {@code false} if the BungeeCordProcess isn't alive
     * @see Process#isAlive()
     * @see Process#getInputStream()
     */
    public boolean isAlive() {
        try {
            return process != null && process.getInputStream().available() != -1 && process.isAlive();
        } catch (final Throwable throwable) {
            return false;
        }
    }

    /**
     * Stops the Process
     *
     * @param message
     * @return {@code true} if the Client could stop the Process or
     * {@code false} if the Client couldn't stop the Process
     * @see ProxyStartupHandler#isAlive()
     */
    public boolean shutdown(final String message) {
        if (message == null)
            this.executeCommand("end KlarCloud restarting...");
        else
            this.executeCommand(message.startsWith(" ") ? "end" + message : "end " + message);

        KlarCloudLibrary.sleep(250);

        if (this.isAlive())
            this.process.destroyForcibly();
        KlarCloudLibrary.sleep(250);

        FileUtils.deleteFullDirectory(path);

        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutRemoveProcess(KlarCloudClient.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByUID(
                this.proxyStartupInfo.getUid()
        )));

        KlarCloudClient.getInstance().getCloudProcessScreenService().unregisterProxyProcess(this.proxyStartupInfo.getName());
        KlarCloudClient.getInstance().getInternalCloudNetwork().getServerProcessManager().unregisterProxyProcess(
                this.proxyStartupInfo.getUid(), this.proxyStartupInfo.getName(), this.port
        );
        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutUpdateInternalCloudNetwork(KlarCloudClient.getInstance().getInternalCloudNetwork()));

        try {
            this.finalize();
        } catch (final Throwable ignored) {
        }

        return true;
    }

    /**
     * Executes a command on the BungeeCordProcess
     *
     * @param command
     * @see Process#getOutputStream()
     * @see OutputStream#write(byte[])
     */
    public void executeCommand(String command) {
        if (!this.isAlive()) return;

        try {
            process.getOutputStream().write((command + "\n").getBytes());
            process.getOutputStream().flush();
        } catch (final IOException ignored) {
        }
    }

    private void downloadViaVersion() {
        DownloadManager.download("ViaVersion", "https://ci.viaversion.com/job/ViaVersion/lastBuild/us.myles$viaversion-bungee/artifact/us.myles/viaversion-bungee/2.0.0-SNAPSHOT/viaversion-bungee-2.0.0-SNAPSHOT.jar", "klarcloud/files/ViaVersion.jar");
    }

    /**
     * Prepares the configuration for the BungeeCord startup
     *
     * @param file
     * @param hostAndPort
     * @throws Throwable
     */
    private void prepareConfiguration(final File file, final String hostAndPort) throws Throwable {
        String context = org.apache.commons.io.FileUtils.readFileToString(file, StandardCharsets.UTF_8);
        context = context.replace("0.0.0.0:25577", hostAndPort);
        org.apache.commons.io.FileUtils.write(file, context, StandardCharsets.UTF_8);
    }

    /**
     * Sends a message to KlarCloudController and to Client Console
     *
     * @param message
     * @see PacketOutSendControllerConsoleMessage
     */
    private void sendMessage(final String message) {
        KlarCloudClient.getInstance().getKlarCloudConsoleLogger().info(message);
        KlarCloudClient.getInstance().getChannelHandler().sendPacketSynchronized("KlarCloudController", new PacketOutSendControllerConsoleMessage(message));
    }

    public String uploadLog() throws IOException {
        if (!this.isAlive())
            return null;

        StringBuilder stringBuilder = new StringBuilder();

        if (this.proxyStartupInfo.getProxyGroup().getProxyVersions().equals(ProxyVersions.BUNGEECORD)
                || this.proxyStartupInfo.getProxyGroup().getProxyVersions().equals(ProxyVersions.HEXACORD)) {
            Files.readAllLines(Paths.get(this.path + "/proxy.log.0"), StandardCharsets.UTF_8).forEach(e -> stringBuilder.append(e).append("\n"));
        } else if (this.proxyStartupInfo.getProxyGroup().getProxyVersions().equals(ProxyVersions.TRAVERTINE)
                || this.proxyStartupInfo.getProxyGroup().getProxyVersions().equals(ProxyVersions.WATERFALL)) {
            Files.readAllLines(Paths.get(this.path + "/logs/latest.log"), StandardCharsets.UTF_8).forEach(e -> stringBuilder.append(e).append("\n"));
        }

        return KlarCloudClient.getInstance().getKlarCloudConsoleLogger().uploadLog(stringBuilder.substring(0));
    }
}
