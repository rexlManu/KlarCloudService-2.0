/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudClient;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.utility.StringUtil;
import de.klarcloudservice.versioneering.VersionController;
import de.klarcloudservice.versioneering.VersionUpdater;

/**
 * @author _Klaro | Pasqual K. / created on 08.01.2019
 */

public final class CommandUpdate implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length == 1) {
            if (VersionController.isVersionAvailable()) {
                try {
                    commandSender.sendMessage("Trying to update the full cloud system....");
                    commandSender.sendMessage("! This will stop KlarCloud !");
                    new VersionUpdater().update();
                    commandSender.sendMessage("Update done");
                } catch (final Throwable throwable) {
                    StringUtil.printError(KlarCloudClient.getInstance().getKlarCloudConsoleLogger(), "An error occurred while updating CloudSystem", throwable);
                }
            } else commandSender.sendMessage("Your version is already up-to-date.");
        } else {
            commandSender.sendMessage("Checking for updates...");
            KlarCloudClient.getInstance().checkForUpdates();
        }
    }

    @Override
    public String getPermission() {
        return "klarcloud.command.update";
    }
}
