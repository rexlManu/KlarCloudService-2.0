/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.in;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.io.Serializable;
import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 29.01.2019
 */

public final class PacketInGetLog implements Serializable, NettyAdaptor {
    private static final long serialVersionUID = -2757983690912958355L;

    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("Log for server \"" + configuration.getStringValue("process") + "\": " + configuration.getStringValue("url"));
    }
}
