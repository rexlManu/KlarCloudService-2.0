/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.utility.StringUtil;

/**
 * @author _Klaro | Pasqual K. / created on 16.12.2018
 */

public final class CommandInfo implements Command {
    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        KlarCloudController.getInstance().getKlarCloudConsoleLogger().info("You are using the KlarCloudVersion " + StringUtil.KLARCLOUD_VERSION + "@" + StringUtil.KLARCLOUD_SPECIFICATION + " by _Klaro");
    }

    @Override
    public String getPermission() {
        return null;
    }
}
