/*
  Copyright © 2019 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudController;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.meta.client.Client;
import de.klarcloudservice.meta.info.ProxyInfo;
import de.klarcloudservice.meta.info.ServerInfo;
import de.klarcloudservice.netty.packets.PacketOutUploadLog;
import de.klarcloudservice.utility.StringUtil;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author _Klaro | Pasqual K. / created on 29.01.2019
 */

public final class CommandLog implements Command, Serializable {
    private static final long serialVersionUID = 8869467720046339074L;

    @Override
    public void executeCommand(CommandSender commandSender, String[] args) {
        if (args.length == 1 && args[0].equalsIgnoreCase("controller")) {
            StringBuilder stringBuilder = new StringBuilder();
            try {
                Files.readAllLines(Paths.get("klarcloud/logs/CloudLog.0")).forEach(s -> stringBuilder.append(s).append("\n"));
            } catch (final IOException ex) {
                StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Could not read log", ex);
            }

            final String url = KlarCloudController.getInstance().getKlarCloudConsoleLogger().uploadLog(stringBuilder.substring(0));
            commandSender.sendMessage("The log for the Controller was uploaded: " + url);
            return;
        }

        if (args.length != 2) {
            commandSender.sendMessage("log <CONTROLLER>");
            commandSender.sendMessage("log <CLIENT, SPIGOT, PROXY> <name>");
            return;
        }

        switch (args[0].toLowerCase()) {
            case "client": {
                Client client = KlarCloudController.getInstance().getInternalCloudNetwork().getClients().get(args[1]);
                if (client != null) {
                    if (!KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(client.getName(),
                            new PacketOutUploadLog(client.getName(), "client"))) {
                        commandSender.sendMessage("This client isn't registered");
                    }
                } else {
                    commandSender.sendMessage("This client doesn't exists");
                }
                break;
            }

            case "spigot": {
                ServerInfo serverInfo = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredServerByName(args[1]);
                if (serverInfo != null) {
                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(serverInfo.getServerGroup().getClient(),
                            new PacketOutUploadLog(serverInfo.getCloudProcess().getName(), "spigot"));
                } else {
                    commandSender.sendMessage("This server isn't registered");
                }
                break;
            }

            case "proxy": {
                ProxyInfo proxyInfo = KlarCloudController.getInstance().getInternalCloudNetwork().getServerProcessManager().getRegisteredProxyByName(args[1]);
                if (proxyInfo != null) {
                    KlarCloudController.getInstance().getChannelHandler().sendPacketAsynchronous(proxyInfo.getProxyGroup().getClient(),
                            new PacketOutUploadLog(proxyInfo.getCloudProcess().getName(), "bungee"));
                } else {
                    commandSender.sendMessage("This proxy isn't registered");
                }
                break;
            }

            default: {
                commandSender.sendMessage("The serverType is invalid");
            }
        }
    }

    @Override
    public String getPermission() {
        return "klarcloud.command.log";
    }
}
