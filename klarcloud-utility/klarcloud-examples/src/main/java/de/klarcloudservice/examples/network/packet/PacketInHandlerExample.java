/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.examples.network.packet;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

public class PacketInHandlerExample implements NettyAdaptor {
    /**
     * Method get called, when the handler gets triggered
     * Contains sent configuration, with all added stuff like String and Integers
     *
     * !! NOTE: {@link QueryType#COMPLETE} gets removed while handling the packet !!
     * !! Dont forget to register the packet handler !!
     */
    @Override
    public void handle(Configuration configuration, List<QueryType> queryTypes) {
    }
}
