/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.examples.network.packet;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.Packet;
import de.klarcloudservice.netty.packet.enums.PacketSender;
import de.klarcloudservice.netty.packet.enums.QueryType;
import de.klarcloudservice.utility.StringUtil;

import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

public class PacketExample extends Packet { //Class can be final
    public PacketExample() {
        super(
                "Example", //Type of the packet, packet will be handled by this name
                new Configuration().addStringProperty("URLClassPath", StringUtil.NULL), //What is in the packet, main message, information
                Collections.singletonList(QueryType.COMPLETE), //Query type(s) of packet !!HAS TO CONTAIN COMPLETE !!
                PacketSender.OTHER //PacketSender of packet
        );
    }
}
