/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.utility.runtime;

/**
 * @author _Klaro | Pasqual K. / created on 19.10.2018
 */

public interface Shutdown {
    void shutdownAll();
}
