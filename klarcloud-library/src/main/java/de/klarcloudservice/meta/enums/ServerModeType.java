/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.enums;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

public enum ServerModeType {
    LOBBY,
    STATIC,
    GAME_SERVER
}
