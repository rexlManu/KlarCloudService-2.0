/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.server.configuration;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

/**
 * @author _Klaro | Pasqual K. / created on 01.11.2018
 */

@AllArgsConstructor
@Getter
public class AdvancedConfiguration implements Serializable {
    private static final long serialVersionUID = -976102679117872951L;

    protected boolean disableNether, enableCommandBlock, spawnMonsters, spawnAnimals, PVP, hardcore, generateStructures, fly, query, announcePlayerAchievements, forceGamemode;
    protected String worldName, resourcePack;
    protected int maxPlayers, viewDistance, idleTimeout, difficulty, defaultGamemode, maxBuildHeight;
}
