/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.meta.proxy.defaults;

import de.klarcloudservice.meta.proxy.configuration.Motd;
import de.klarcloudservice.meta.proxy.configuration.ProxyConfig;
import de.klarcloudservice.meta.proxy.configuration.ProxyMaintenanceConfig;
import de.klarcloudservice.meta.proxy.configuration.Tablist;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;

/**
 * @author _Klaro | Pasqual K. / created on 21.10.2018
 */

public final class DefaultProxyConfig extends ProxyConfig implements Serializable {
    private static final long serialVersionUID = 5559609589811160975L;

    public DefaultProxyConfig(String host) {
        super(true, false, false, false,
                new Motd(true, "                §8» §2§lKlar§f§lCloud§2§lService §8«",
                        "              §8【● §fOfficial §2Cloud §fSystem §8●】"),
                512, Arrays.asList(
                        "§8§m---------------------------------------------",
                        "§r                   §8» §2§lKlar§f§lCloud§2§lService §8«",
                        "                     §fOfficial §2Cloud §fSystem ",
                        " ",
                        "§r                  §8§m----------------------",
                        " ",
                        "                      §fTwitter §8» §f@§2KlarCloud",
                        "                §fDiscord §8» §fdiscord.gg§2/§ffwe2CHD",
                        "             §fTeamSpeak §8» §2ts§f.§2KlarCloudService§f.§2de",
                        "§8§m---------------------------------------------"
                ), host, "§8【● §2%online%§f/§2%max% §8●】", Collections.emptyList(),
                new Tablist(true, 1,
                        "§c§lK§4§llar§c§lC§4§lloud§c§lS§4§lervice§8│ §7An §4intelligent §7CloudSystem",
                        "§8➥ §7Version §8» §4%version%§8│ §7Proxy §8» §4%proxy%")
                , new ProxyMaintenanceConfig(new Motd(true,
                        "\n      §2§lKlar§f§lCloud§2§lService §8● §fOfficial §2Cloud §fSystem \n\n §fProxy §8» §2%proxy%§8│ §fServer §8» §2%server% \n §fClient §8» §2%client%§8│ §fPlayers §8» §2%online%§f/§2%max% \n",
                        "\n §fTwitter §8» §f@§2KlarCloud \n §fDiscord §8» §fdiscord.gg§2/§ffwe2CHD \n §fTeamSpeak §8» §2ts§f.§2KlarCloudService§f.§2de \n"),
                        "§8【● §2§lMaintenance §8●】",
                        Arrays.asList(
                                "§8§m---------------------------------------------",
                                "§r                   §8» §2§lKlar§f§lCloud§2§lService §8«",
                                "                     §fOfficial §2Cloud §fSystem ",
                                " ",
                                "§r                  §8§m----------------------",
                                " ",
                                "                      §fTwitter §8» §f@§2KlarCloud",
                                "                §fDiscord §8» §fdiscord.gg§2/§ffwe2CHD",
                                "             §fTeamSpeak §8» §2ts§f.§2KlarCloudService§f.§2de",
                                "§8§m---------------------------------------------"
                        )));
    }
}
