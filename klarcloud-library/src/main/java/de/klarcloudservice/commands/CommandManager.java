/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.commands;

import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.KlarCloudLibraryService;
import de.klarcloudservice.commands.defaults.KlarCloudCommandSender;
import de.klarcloudservice.commands.defaults.KlarCloudUserCommandSender;
import de.klarcloudservice.commands.interfaces.Command;
import de.klarcloudservice.commands.interfaces.CommandSender;
import de.klarcloudservice.utility.StringUtil;

import java.util.Map;
import java.util.Set;

/**
 * @author _Klaro | Pasqual K. / created on 18.10.2018
 */

/**
 * CommandManager handles all {@link Command}
 */
public class CommandManager {
    private final KlarCloudCommandSender klarCloudCommandSender = new KlarCloudCommandSender();
    private Map<String, Command> commandMap = KlarCloudLibrary.concurrentHashMap();

    /**
     * Dispatch method, to dispatch command
     *
     * @param commandSender
     * @param command
     * @return if the command is registered or not
     */
    private boolean dispatchCommand(CommandSender commandSender, String command) {
        String[] strings = command.split(" ");

        if (strings.length <= 0) return false;

        if (this.commandMap.containsKey(strings[0].toLowerCase()) && commandSender.hasPermission(this.commandMap.get(strings[0].toLowerCase()).getPermission())) {
            String string = command.replace((command.contains(" ") ? command.split(" ")[0] + " " : command), "");
            try {
                if (string.equalsIgnoreCase(""))
                    this.commandMap.get(strings[0].toLowerCase()).executeCommand(commandSender, new String[0]);
                else {
                    final String[] arguments = string.split(" ");
                    this.commandMap.get(strings[0].toLowerCase()).executeCommand(commandSender, arguments);
                }
            } catch (final Throwable throwable) {
                StringUtil.printError(KlarCloudLibraryService.getInstance().getKlarCloudConsoleLogger(), "Error while dispatching command", throwable);
            }
            return true;
        } else
            return false;
    }

    /**
     * Register a command
     *
     * @param name
     * @param command
     * @return this
     */
    public CommandManager registerCommand(final String name, Command command) {
        this.registerCommand(name, command, new String[]{});
        return this;
    }

    /**
     * Register a command with aliases
     *
     * @param name
     * @param command
     * @param aliases
     * @return this
     */
    public CommandManager registerCommand(final String name, Command command, final String... aliases) {
        this.commandMap.put(name.toLowerCase(), command);

        if (aliases.length > 0)
            for (String alias : aliases)
                this.commandMap.put(alias.toLowerCase(), command);

        return this;
    }

    /**
     * Unregisters all commands
     */
    public void clearCommands() {
        this.commandMap.clear();
    }

    /**
     * Unregisters an specific command
     *
     * @param name
     */
    public void unregisterCommand(final String name) {
        this.commandMap.remove(name);
    }

    /**
     * Check if an command is registered
     *
     * @param command
     * @return if the command is registered
     */
    public boolean isCommandRegistered(final String command) {
        return this.commandMap.containsKey(command);
    }

    /**
     * Gets all registered Commands
     *
     * @return a Set with all commands as String
     */
    public Set<String> getCommands() {
        return this.commandMap.keySet();
    }

    /**
     * Dispatch a command with the default {@link KlarCloudCommandSender}
     *
     * @param command
     * @return if the command is registered
     */
    public boolean dispatchCommand(String command) {
        return this.dispatchCommand(this.klarCloudCommandSender, command);
    }

    /**
     * Creates a new {@link KlarCloudUserCommandSender}
     *
     * @param permissions
     * @return new {@link KlarCloudUserCommandSender} with given permissions
     */
    public CommandSender newCommandSender(final Map<String, Boolean> permissions) {
        return new KlarCloudUserCommandSender(permissions);
    }
}
