/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.logging.handlers;

/**
 * @author _Klaro | Pasqual K. / created on 19.10.2018
 */

/**
 * Register a Handler in {@link de.klarcloudservice.logging.KlarCloudConsoleLogger} to get the console input
 */
public interface IConsoleInputHandler {
    void handle(String message);
}
