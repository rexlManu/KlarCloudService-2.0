/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packet.enums;

/**
 * @author _Klaro | Pasqual K. / created on 27.10.2018
 */

public enum QueryType {
    QUERY,
    RESULT,
    FIRE_EXCEPTION,
    NO_RESULT,
    COMPLETE
}
