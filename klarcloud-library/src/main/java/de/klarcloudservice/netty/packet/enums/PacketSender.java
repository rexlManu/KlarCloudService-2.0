/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.packet.enums;

/**
 * @author _Klaro | Pasqual K. / created on 27.12.2018
 */

public enum  PacketSender {
    CONTROLLER,
    CLIENT,
    PROCESS_SERVER,
    PROCESS_PROXY,
    OTHER
}
