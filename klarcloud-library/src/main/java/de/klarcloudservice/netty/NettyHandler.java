/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty;

import de.klarcloudservice.KlarCloudLibrary;
import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.interfaces.NettyAdaptor;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author _Klaro | Pasqual K. / created on 18.10.2018
 */

public class NettyHandler {
    private Map<String, NettyAdaptor> nettyAdaptorMap = KlarCloudLibrary.concurrentHashMap();

    /**
     * Handel the incoming packet by calling the registered {@link NettyAdaptor}
     * default call by {@link de.klarcloudservice.netty.channel.ChannelReader}
     *
     * @param type
     * @param configuration
     * @param queryTypes
     * @return if the Handler is registered
     */
    public boolean handle(String type, Configuration configuration, List<QueryType> queryTypes) {
        if (this.nettyAdaptorMap.containsKey(type)) {
            this.nettyAdaptorMap.get(type).handle(configuration, queryTypes);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Clears all Handlers
     */
    public void clearHandlers() {
        this.nettyAdaptorMap.clear();
    }

    /**
     * Returns the registered {@link NettyAdaptor}
     *
     * @param type
     * @return the registered {@link NettyAdaptor} or null if the Handler isn't registered
     */
    public NettyAdaptor getHandler(String type) {
        return nettyAdaptorMap.getOrDefault(type, null);
    }

    /**
     * Registers a new {@link NettyAdaptor}
     *
     * @param type
     * @param nettyAdaptor
     */
    public NettyHandler registerHandler(String type, NettyAdaptor nettyAdaptor) {
        this.nettyAdaptorMap.put(type, nettyAdaptor);
        return this;
    }

    /**
     * Unregisters a specific {@link NettyAdaptor}
     *
     * @param type
     */
    public NettyHandler unregisterHandler(String type) {
        this.nettyAdaptorMap.remove(type);
        return this;
    }

    /**
     * Get all names of all registered {@link NettyAdaptor}
     *
     * @return {@link Set<String>} with HandleName of all {@link NettyAdaptor}
     */
    public Set<String> getHandlers() {
        return this.nettyAdaptorMap.keySet();
    }

    /**
     * Returns if a specific {@link NettyAdaptor} is registered by name
     *
     * @param name
     * @return if the handler is registered
     */
    public boolean isHandlerRegistered(final String name) {
        return this.nettyAdaptorMap.containsKey(name);
    }
}
