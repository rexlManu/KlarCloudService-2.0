/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.netty.interfaces;

import de.klarcloudservice.configurations.Configuration;
import de.klarcloudservice.netty.packet.enums.QueryType;

import java.util.List;

/**
 * @author _Klaro | Pasqual K. / created on 18.10.2018
 */

public interface NettyAdaptor {
    void handle(Configuration configuration, List<QueryType> queryTypes);
}
