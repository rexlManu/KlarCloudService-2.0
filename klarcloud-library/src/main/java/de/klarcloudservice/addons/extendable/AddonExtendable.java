/*
  Copyright © 2018 Pasqual K. | All rights reserved
 */

package de.klarcloudservice.addons.extendable;

/**
 * @author _Klaro | Pasqual K. / created on 10.12.2018
 */

public abstract class AddonExtendable {
    public abstract void loadAddons();

    public abstract void enableAddons();

    public abstract void disableAddons();
}
